var server_config = require('./config/server_config');
var mongoose = require('mongoose');

mongoose.connect(server_config.MONGO_URI).then(() => {
    console.log("Connected at MongoDB -> " + server_config.MONGO_URI);
    require('./core/server');
}).catch((err) => {
    console.log("An error ocurred when connecting to MongoDB -> " +  err);
});
