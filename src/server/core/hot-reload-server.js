/* Current server in user by ProdigyApp, it will act like a proxy to the WebpackDevServer */

var server = require("./server");

var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('../../../webpack.config.hot-reload');



new WebpackDevServer(webpack(config), {

    publicPath: config.output.publicPath,
    hot: true,
    historyApiFallback: true,
    proxy: {
        "*": "http://localhost:9001"
    }
}).listen(9002, 'localhost', (err, result) => {
    if (err) {
        return console.log(err);
    }

    console.log('Webpack Dev Server listening at @9002');
});
