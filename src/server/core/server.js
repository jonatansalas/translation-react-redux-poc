var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var contentful = require('contentful');
var util = require('util');
var Q = require('q');
var services = require('../service/services.js');

var app = express();

var server_config = require('../config/server_config');

app.set('views', './src/public/views');
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

app.use(morgan('dev'));

app.use(express.static(server_config.PUBLIC_STATIC_CONTENT_DIR));

var cmsClient = contentful.createClient({
  // This is the space ID. A space is like a project folder in Contentful terms
  space: 'yv98ozite9o1',
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  accessToken: '210aacb03a0c673ffc194673e71437e8b7600b592dd0f936c45849a69234dd96'
});

var router = express.Router();

router.get("/", function(request, response) {
   response.sendFile("index.html", {root: server_config.PUBLIC_STATIC_CONTENT_DIR});
});

router.get("/structure", function(req, res) {
	res.json(
		{
			'locales':['en', 'es'],
			'components':{
				'firstComponent':{
					'en': {
						'texts':{
							'title':'First component',
							'summary':'First summary'
						}
					},
					'es': {
						'texts':{
							'title':'Primer componente',
							'summary':'Primer resumen'
						}
					}			
				},
				'secondComponent':{
					'en': {
						'texts':{
							'title':'Second component',
							'summary':'Second summary'
						}
					},
					'es': {
						'texts':{
							'title':'Segundo componente',
							'summary':'Segundo resumen'
						}
					}			
				}
		}, 
		'selectedLocale':'en'
		}
	);
});

router.get("/translate/:locale/:componentId", Q.async(services.getTranslations));

router.get("/cmsStructure", function(req, res) {
	cmsClient.getEntry('4MNEXF7DPGEK2QwwgUSOac')
	.then(function (entry) {
		console.log('getting entry 4MNEXF7DPGEK2QwwgUSOac');	
		console.log(util.inspect(entry, {depth: null}));
		res.json(entry);
	}, function(err){
		console.log(err);
	});
});

app.use(router);

app.listen(server_config.PORT, function () {
    console.log("Node server listening @ " + server_config.PORT);
});

module.exports = app;		