const fs = require('fs');

module.exports = {
    getFile: (requestData, callback) => {
        const localeDir = 'src/assets/locales/' + requestData.locale;

        fs.exists(localeDir, (exists) => {
            if (exists) {
                const componentName = String(requestData.componentId).toLowerCase();
                const path = localeDir + '/' + componentName + '.json';
                fs.readFile(path, (error, data) => {
                    if (error) {
                        callback(new Error("Specified component name as path doesn't exits"));
                    } else {
                        callback(null, data);
                    }
                });
            } else {
                callback(new Error("Specified locale as path doesn't exist"));
            }
        });
    }
};