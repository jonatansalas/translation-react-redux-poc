const TranslationProviderActions = require('../action/translation_provider_actions.js');
const FileTranslationProvider = require('../service/file_translation_provider.js');

module.exports = {
    get: (request, response, actionType) => {
        switch (actionType) {
            case TranslationProviderActions.FROM_PROPERTY_FILE:

                FileTranslationProvider.getFile(request.params, (error, data) => {
                    if (error) {
                        response.status(404).json({error: String(error)});
                        return;
                    }

                    response.status(200);
                    response.send(JSON.parse(data));
                });

                break;
            case TranslationProviderActions.FROM_DATABASE:
                break;
            case TranslationProviderActions.FROM_CMS:
                break;
            default:
                break;
        }
    }
};