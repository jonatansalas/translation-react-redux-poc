const TranslationProviderActions = require('../action/translation_provider_actions');
const TranslationProvider = require('../service/translation_provider');

module.exports = {
    getTranslations: (request, response) => {
        TranslationProvider.get(request, response, TranslationProviderActions.FROM_PROPERTY_FILE);
    }
};