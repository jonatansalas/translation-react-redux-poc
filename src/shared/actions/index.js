import fetch from 'isomorphic-fetch'

export const CHANGE_LOCALE = 'CHANGE_LOCALE';
export const RECEIVE_STRUCTURE = 'RECEIVE_STRUCTURE';

export const changeLocale = (locale) => {
  return {
    type: CHANGE_LOCALE,
    locale: locale
  }
};
 
const receiveStructure = (structure) => {
  return {
    type: RECEIVE_STRUCTURE,
	'structure': structure
  }
};

//async action. with Thunk middleware applied to our store we can dispatch async actions.
export const fetchStructure = () => {
  return dispatch => {
    return fetch('http://localhost:9001/structure')
      .then(response => response.json())
      .then(structure => dispatch(receiveStructure(structure)))
  }
};