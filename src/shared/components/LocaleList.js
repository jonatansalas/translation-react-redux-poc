import React, { Component } from 'react'

export default class LocaleList extends Component {
  render() {

	var options = this.props.options.map((opt, i) => {
		return <option key={i} value={opt} label={opt}>{opt}</option>;
	}, this);

	return(
		<div>
			<p>Locale:</p>
			<select onChange={this.props.doChange}>
				{options}
			</select>
		</div>
	)

  }
}