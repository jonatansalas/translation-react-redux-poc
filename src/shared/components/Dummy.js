import React, { Component } from 'react'

export default class Dummy extends Component {
  render() {
    return (
      <div>
        <h1>{this.props.texts ? this.props.texts.title : ''}</h1>
        <p>Summary: {this.props.texts ? this.props.texts.summary : ''}</p>
      </div>
    )
  
  }
}