import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'
import Dummy from '../components/Dummy'
import LocaleList from '../components/LocaleList'
import { fetchStructure, changeLocale } from '../actions'

class CMSPoc extends Component {

	constructor(props) {
    	super(props)
 	}

	componentDidMount() {
		console.log('fetching');
		//dispatch async action to get structure from contentful.
    	this.props.fetchIt();
 	}

	render() {

	    const { components, selectedLocale, locales, isFetching } = this.props;

		return (
			<div>
				<LocaleList options={locales ? locales : []} doChange={this.props.handleChangeLocale} />
				<Dummy texts={components ? components.firstComponent[selectedLocale].texts : null}/>
				<Dummy texts={components ? components.secondComponent[selectedLocale].texts : null}/>
			</div>
		)

	}
}

var mapStateToProps = function(state){
    return {
    	components : state.structure.components,
    	selectedLocale : state.structure.selectedLocale,
    	locales: state.structure.locales,
    	isFetching: state.isFetching
    };
};

var mapDispatchToProps = (dispatch) => {
    return {
        handleChangeLocale: (e) => {
        	var locale = e.target.value;
        	console.log('@Root: dispatching changeLocale(' + locale + ')');
        	dispatch(changeLocale(locale)); 
        },
        fetchIt: () => {
			dispatch(fetchStructure());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CMSPoc)