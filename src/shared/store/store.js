import combinedReducers from '../reducers/combined_reducers';
import { createStore } from 'redux';

const store = createStore(combinedReducers);
