import TranslateActions from '../actions/translate_actions';
import Winston from 'winston';

const translationReducer = (state = { locales: ["en", "es"], fallbackLocale: "en" }, action) => {
    switch (action.type) {
        case TranslateActions.ACTION_FETCH_TRANSLATIONS:
            Winston.log("info", "@TranslationReducer => calling action fetch translations");
            return state;
        case TranslateActions.ACTION_TRANSLATE:
            const locale = action.locale;
            Winston.log("info", "@TranslationReducer => calling action translate for locale: " + locale);
            return state; 
        default:
            Winston.log("info", "@TranslationReducer => calling default action");
            return state; 
    }
};

export default translationReducer;