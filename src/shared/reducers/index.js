import { combineReducers } from 'redux'
import {
	CHANGE_LOCALE,
	RECEIVE_STRUCTURE
} from '../actions'

//initial state with empty structure.
const structure = (state = {'isFetching': true} , action) => {
  switch (action.type) {
    case CHANGE_LOCALE:
    	console.log('@reducer: state changed! selectedLocale: ' + action.locale);
		return Object.assign({}, state, {
			'selectedLocale': action.locale
        });
    case RECEIVE_STRUCTURE:
		console.log('@reducer: state changed! structure: ' + action.structure.components);
		return action.structure;
	default:
		return state
  }
};

//no sense to combine as we have only one, anyway leaving it..
const rootReducer = combineReducers({ structure });

export default rootReducer;