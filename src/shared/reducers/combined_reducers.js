import translationReducers from '../reducers/translation_reducer';
import { combineReducers } from 'redux';

//Here we will import and combine all the reducers we define 
const reducers = combineReducers({ translationReducers });
export default reducers;